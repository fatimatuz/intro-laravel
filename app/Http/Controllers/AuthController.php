<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('tugas12.register');
    }
    public function welcome(Request $request){
        // dd($request->all());
        $nama1 = $request->namadepan;
        $nama2 = $request->namaakhir;
        return view('tugas12.welcome', compact('nama1','nama2'));
    }
}
