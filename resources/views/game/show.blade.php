@extends('layout.master')

@push('rightnav')
@include('partial.rightnav')
@endpush

@section('judul')
Detail Data
@endsection

@section('subjudul')
Detail Data Game: {{$game->name}}
@endsection

@section('content')

<h3>{{$game->name}}</h3>
<p>Gameplay: {{$game->gameplay}}</p>
<p>Developer: {{$game->developer}}</p>
<p>Tahun: {{$game->year}}</p>

@endsection