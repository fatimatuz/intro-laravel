@extends('layout.master')

@push('rightnav')
@include('partial.rightnav')
@endpush

@section('judul')
Create Data
@endsection

@section('subjudul')
Create Data Game
@endsection

@section('content')

<div>
    <form action="/game" method="POST">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Gameplay</label>
                <textarea name="gameplay" class="form-control" cols="30" rows="10" placeholder="Masukkan gameplay"></textarea>
                @error('gameplay')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Developer</label>
                <input type="text" class="form-control" name="developer" placeholder="Masukkan Nama Developer">
                @error('developer')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tahun</label>
                <input type="integer" class="form-control" name="year" placeholder="Masukkan Tahun Pengembangan">
                @error('year')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection