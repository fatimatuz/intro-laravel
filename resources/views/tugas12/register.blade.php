@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('subjudul')
Sign Up Form
@endsection

@section('content')
<form action="/welcome" method="post">
@csrf
<label for="first_name">First name:</label> <br><br>
<input type="text" name="namadepan" placeholder="First Name" id="first_name"> <br><br>
<label for="last_name">Last name:</label> <br><br>
<input type="text" name="namaakhir" placeholder="Last Name" id="last_name"> <br><br>
<label>Sex:</label> <br><br>
<input type="radio" name="sex" value="1">Male <br>
<input type="radio" name="sex" value="2">Female <br><br>
<label>Nationality:</label> <br><br>
<select>
<option>Indonesian</option>
<option>Afghan</option>
<option>Australian</option>
<option>Brazilian</option>
<option>Egyptian</option>
<option>English</option>
<option>Japanese</option>
<option>Malaysian</option>
<option>Saudi</option>
<option>Singaporean</option>
</select>
<br><br>
<label>Language Spoken:</label> <br><br>
<input type="checkbox" name="language" value="1">Bahasa Indonesia <br>
<input type="checkbox" name="language" value="2">English <br>
<input type="checkbox" name="language" value="3">Other <br><br>
<label for="bio">Bio:</label> <br><br>
<textarea cols="30" rows="8" id="bio"> </textarea> <br><br>
<input type="submit" value="Sign Up">
</form>
@endsection