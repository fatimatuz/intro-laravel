@extends('layout.master')

@section('judul')
Selamat Datang!
@endsection
@section('subjudul')
SELAMAT DATANG! {{$nama1." ".$nama2}}
@endsection

@section('content')
<h5> Terima kasih telah bergabung di Website kami. Media Belajar kita bersama! </h5>
@endsection