@extends('layout.master')

@push('rightnav')
@include('partial.rightnav')
@endpush

@section('judul')
List Cast
@endsection

@section('subjudul')
Halaman Data Pemain Film
@endsection

@section('content')
@auth
<a href="/cast2/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                        @guest
                        <a href="/cast2/{{$value->id}}" class="btn btn-info">Detail</a>
                        @endguest
                        @auth
                            <form action="/cast2/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/cast2/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/cast2/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('Apakah kamu yakin hapus?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        @endauth
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Tidak ada data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection