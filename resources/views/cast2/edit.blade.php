@extends('layout.master')

@push('rightnav')
@include('partial.rightnav')
@endpush

@section('judul')
Edit Cast
@endsection

@section('subjudul')
Form Edit Data Pemain: {{$cast->nama}}
@endsection

@section('content')

<div>
    <form action="/cast2/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="integer" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label>
                <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio" value="{{$cast->bio}}"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save Edit</button>
        </form>
</div>
@endsection