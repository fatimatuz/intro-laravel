@extends('layout.master')

@push('rightnav')
@include('partial.rightnav')
@endpush

@section('judul')
Detail Cast
@endsection

@section('subjudul')
Detail Pemain: {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary">Kembali</a>

@endsection